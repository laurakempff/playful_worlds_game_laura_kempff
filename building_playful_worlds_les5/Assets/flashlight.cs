﻿
using UnityEngine;

public class flashlight : MonoBehaviour
{
    public Light flashlight_light;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F) && flashlight_light.enabled == false)
        {
            flashlight_light.enabled = true;
        }
        else if (Input.GetKeyDown(KeyCode.F) && flashlight_light.enabled == true)
        {
            flashlight_light.enabled = false;
        }

    }
}
