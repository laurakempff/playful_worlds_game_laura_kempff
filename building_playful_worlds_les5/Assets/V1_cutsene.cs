﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class V1_cutsene : MonoBehaviour

{
    public GameObject ThePlayer;
    public GameObject cutscenes_V1;
    public GameObject particle;
    public GameObject rain_sound;
    public GameObject BG_sound;

    void OnTriggerEnter(Collider other)
    {
        this.gameObject.GetComponent<BoxCollider>().enabled = false;
        particle.SetActive(true);
        rain_sound.SetActive(true);
        BG_sound.SetActive(false);
        cutscenes_V1.SetActive(true);
        ThePlayer.SetActive(false);
        StartCoroutine(FinishCut());
    }

    IEnumerator FinishCut()
    {
        yield return new WaitForSeconds(23);
        //particle.SetActive(false);
        //rain_sound.SetActive(false);
        BG_sound.SetActive(false);
        rain_sound.SetActive(true);
        particle.SetActive(true);
        ThePlayer.SetActive(true);
        rain_sound.GetComponent<AudioSource>().Play();
        cutscenes_V1.SetActive(false);
    }

}
