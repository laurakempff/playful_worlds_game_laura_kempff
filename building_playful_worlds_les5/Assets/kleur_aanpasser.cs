﻿
using UnityEngine;

public class kleur_aanpasser : MonoBehaviour
{
    public Color kleur;
    public GameObject sphere;
    public GameObject paal;
    void OnCollisionEnter(Collision collisionInfo)
    {
        if (collisionInfo.collider.CompareTag("Player"))
        {
            paal.GetComponent<MeshRenderer>().material.color = kleur;
            sphere.GetComponent<MeshRenderer>().material.color = kleur;
        }
    }
}
